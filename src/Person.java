package JavaDesktopApp;

import java.io.*;

@SuppressWarnings("serial")
public class Person implements Serializable {

    private String firstName;
    private String familyName;
    private String nationality;
    private String profession;

    /**
     * Create a new person.
     */
    public Person() {
        this.firstName = "Unknown";
        this.familyName = "Unknown";
        this.nationality = "None";
        this.profession = "None";

    }

    /**
     * Create a new person with certain information.
     *
     * @param firstName the first name of the person
     * @param familyName the family name of the person
     * @param nationality the nationality of the person
     * @param profession the profession of the person
     */
    public Person(String firstName, String familyName,
        String nationality, String profession) {
        this.firstName = firstName;
        this.familyName = familyName;
        this.nationality = nationality;
        this.profession = profession;
    }

    /**
     * Sets first name of a person.
     *
     * @param firstName the first name to set
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

     /**
     * Sets family name of a person.
     *
     * @param familyName the family name to set
     */
    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    /**
     * Sets nationality of a person.
     *
     * @param nationality the nationality to set
     */
    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    /**
     * Sets profession of a person.
     *
     * @param profession the profession to set
     */
    public void setProfession(String profession) {
        this.profession = profession;
    }

    /**
     * Gets a first name of a person.
     *
     * @return the first name of the person
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Gets a family name of a person.
     *
     * @return the family name of the person
     */
    public String getFamilyName() {
        return familyName;
    }

    /**
     * Gets a nationality of a person.
     *
     * @return the nationality of the person
     */
    public String getNationality() {
        return nationality;
    }

    /**
     * Gets a profession of a person.
     *
     * @return the profession of the person
     */
    public String getProfession() {
        return profession;
    }

    @Override
    public String toString() {
        String info;
        info = "First name: " + getFirstName() + "\n"
                + "Family name: " + getFamilyName() + "\n"
                + "Nationality: " + getNationality() + "\n"
                + "Profession: " + getProfession() + "\n";
        return info;
  }
}
