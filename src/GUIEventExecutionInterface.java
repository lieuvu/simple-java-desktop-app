package JavaDesktopApp;

/**
 * The interface to specify events for GUI.
 */
public interface GUIEventExecutionInterface  {

    public void openToWrite();
    public void write();
    public void openToRead();
    public void readNext ();

}
