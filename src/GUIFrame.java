package JavaDesktopApp;

import javax.swing.*;
import java.awt.*;

@SuppressWarnings("serial")
public class GUIFrame extends JFrame {

    private GUIComponents GUIObj;
    private GUIEventExecution eventExecution;

    /**
     * Create GUI frame.
     */
    public GUIFrame(){
        GUIObj = new GUIComponents();

        //Create Layout Container
        Container contentPane = getContentPane();

        /*Adding Input Panel, OutputScreen and Button Panel to content Pane
            using BorderLayout*/
        contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));
        contentPane.add(GUIObj.getInputPanel());
        contentPane.add(GUIObj.getOutputScreen());
        contentPane.add(GUIObj.getButtonPanel());
        this.setTitle("Application");
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setSize(350,400);

        // Create event execution object
        eventExecution = new GUIEventExecution(GUIObj);
    }
}
