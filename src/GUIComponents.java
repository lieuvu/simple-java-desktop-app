package JavaDesktopApp;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import javax.swing.BorderFactory;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import java.awt.*;

@SuppressWarnings("serial")
public class GUIComponents extends JPanel {

    // Labels
    private JLabel lblFirstName = new JLabel("First name");
    private JLabel lblFamilyName = new JLabel("Family name");
    private JLabel lblNationality = new JLabel("Nationality");
    private JLabel lblProfession = new JLabel("Profession");

    // Text fields
    private JTextField tfFirstName = new JTextField();
    private JTextField tfFamilyName = new JTextField();

    // Combo box
    private JComboBox<String> cbxNationality;
    private JComboBox<String> cbxProfession;

    // Buttons
    private JButton[] btn = new JButton[4];

    // Input panel
    private JPanel inputPanel = new JPanel();

    // Output screen
    private JTextArea txtArea = new JTextArea();
    private JScrollPane outputScreen = new JScrollPane(txtArea);

    // Button pane and put buttons as gridlayout
    private  JPanel btnPane = new JPanel(new GridLayout(2,2,5,5));

    /**
     * Create GUI components.
     */
    public GUIComponents() {

        // Add selections to Combox Box
        String[] txtNationality = {"None", "Chinese", "Finnish", "Nigerian",
                                        "Pakistani", "Vietnamese", "Russian"};
        String[] txtProfession = {"None","Accountant", "Engineer", "Nurse",
                                    "Student", "Teacher", "Seller"};

        cbxNationality = new JComboBox<String>(txtNationality);
        cbxProfession = new JComboBox<String>(txtProfession);

        // Set max size for text field and combo box
        tfFirstName.setMaximumSize(new Dimension(150,30));
        tfFamilyName.setMaximumSize(new Dimension(150,30));
        cbxNationality.setMaximumSize(new Dimension(150,30));
        cbxProfession.setMaximumSize(new Dimension(150,30));

        //set border for input panel
        inputPanel.setBorder(BorderFactory.createEmptyBorder(20,10,0,10));

        //Create group layout manager to input panel
        GroupLayout layout = new GroupLayout(inputPanel);
        inputPanel.setLayout(layout);
        layout.setAutoCreateGaps(true);
        layout.setAutoCreateContainerGaps(true);

        //Adding input components horizontally to input panel
        GroupLayout.SequentialGroup hGroup = layout.createSequentialGroup();
        layout.setHorizontalGroup(hGroup);
        hGroup.addGroup(layout.createParallelGroup()
                                .addComponent(lblFirstName)
                                .addComponent(lblFamilyName)
                                .addComponent(lblNationality)
                                .addComponent(cbxNationality)
        );
        hGroup.addGroup(layout.createParallelGroup()
                                .addComponent(tfFirstName)
                                .addComponent(tfFamilyName)
                                .addComponent(lblProfession)
                                .addComponent(cbxProfession)
        );

        //Adding input components vertically to input panel
        GroupLayout.SequentialGroup vGroup = layout.createSequentialGroup();
        layout.setVerticalGroup(vGroup);
        vGroup.addGroup(layout.createParallelGroup(Alignment.BASELINE)
                                .addComponent(lblFirstName)
                                .addComponent(tfFirstName)
        );
        vGroup.addGroup(layout.createParallelGroup(Alignment.BASELINE)
                                .addComponent(lblFamilyName)
                                .addComponent(tfFamilyName)
        );
        vGroup.addGroup(layout.createParallelGroup(Alignment.BASELINE)
                                .addComponent(lblNationality)
                                .addComponent(lblProfession)
        );
        vGroup.addGroup(layout.createParallelGroup(Alignment.BASELINE)
                                .addComponent(cbxNationality)
                                .addComponent(cbxProfession)
        );

        // Set size and border for outputScreen
        outputScreen.setMaximumSize(new Dimension(325,150));
        outputScreen.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));

        // Set names for buttons
        String[] txtbutton = {"Open for writing", "Write this",
                            "Open for reading", "Read next"};

        for (int i = 0; i < txtbutton.length; i++){
            btn[i] = new JButton(txtbutton[i]);
            btnPane.add(btn[i]);
        }

        // Set size and border for button Panel
        btnPane.setMaximumSize(new Dimension(325,70));
        btnPane.setBorder(BorderFactory.createEmptyBorder(10,10,20,10));
      }

    /**
     * Returns the input panel.
     *
     * @return the input panel
     */
    public JPanel getInputPanel (){
      return inputPanel;
    }

    /**
     * Returns the output screen.
     *
     * @return the output screen
     */
    public JScrollPane getOutputScreen(){
      return outputScreen;
    }

    /**
     * Returns the first name text field.
     *
     * @return the first name text field
     */
    public JTextField getTextfieldFirstName() {
      return tfFirstName;
    }

    /**
     * Returns the family name text field.
     *
     * @return the family name text field
     */
    public JTextField getTextfieldFamilyName() {
      return tfFamilyName;
    }

    /**
     * Returns the nationality combo box.
     *
     * @return the nationality combo box
     */
    public JComboBox<String> getCbxNationality() {
      return cbxNationality;
    }

    /**
     * Returns the profession combo box.
     *
     * @return the profession combo box
     */
    public JComboBox<String> getCbxProfession() {
      return cbxProfession;
    }

    /**
     * Returns the button panel.
     *
     * @return the the button panel
     */
    public JPanel getButtonPanel(){
      return btnPane;
    }

    /**
     * Returns a button at a certain index.
     *
     * @param i The index of a button
     * @return the button at the certain index
     */
    public JButton getBtn(int i) {
      return btn[i];
    }

    /**
     * Returns a text area.
     *
     * @retrn the text area
     */
    public JTextArea getTxtArea() {
	 return txtArea;
    }
}
