package JavaDesktopApp;

import java.io.File;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.EOFException;

import javax.swing.JOptionPane;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class GUIEventExecution implements GUIEventExecutionInterface {

    // Constants
    private final String FILE_EXTENSION = ".ser";

    // Instance properties
    private Person person;
    private GUIComponents GUIComponents;
    private File file;
    private ObjectOutputStream objectOut;
    private ObjectInputStream objectIn;
    private String readingFilePath;

    /**
     * Constructor for class GUIEventExecution.
     *
     * @param GUIComponents
     */
    public GUIEventExecution(GUIComponents GUIComponents) {
        this.GUIComponents = GUIComponents;
        //Disable button "Write this" and "Read next"
        GUIComponents.getBtn(1).setEnabled(false);
        GUIComponents.getBtn(3).setEnabled(false);

	   //Action when click button "Open for Writing"
        GUIComponents.getBtn(0).addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                openToWrite();
            }
        });

        //Action when click button "Write this"
        GUIComponents.getBtn(1).addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                write();
            }
        });

        //Action when click button "Open for reading"
        GUIComponents.getBtn(2).addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                openToRead();
            }
        });

	   //Action when click button "Read next"
        GUIComponents.getBtn(3).addActionListener(new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
              readNext();
          }
      });
    }

    @Override
    public void openToWrite() {
        String fileName;
        fileName = JOptionPane.showInputDialog("Enter the file name: ");

        // Enable input fields, clear text area display
        setInputFieldsEnabled(true);
        GUIComponents.getTxtArea().setText("");

        if (isFileNameValid(fileName)){
            file = new File(fileName + FILE_EXTENSION);

            try{
                objectOut = new ObjectOutputStream(new FileOutputStream(file));
                if (objectIn != null) {
                    objectIn.close();
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }

            JOptionPane.showMessageDialog(null, "You open the file to write at " + file.getAbsolutePath());

            //Set button "Open for Writing" disabled and "Write this" enabled
            //button "Read next" disabled
            GUIComponents.getBtn(0).setEnabled(false);
            GUIComponents.getBtn(1).setEnabled(true);
            GUIComponents.getBtn(3).setEnabled(false);
        }
        else {
            JOptionPane.showMessageDialog(null, "You do not open any file to write!");
        }
    }

    @Override
    public void write() {
        // Get data from input fields
        String firstName = GUIComponents.getTextfieldFirstName().getText();
        String familyName = GUIComponents.getTextfieldFamilyName().getText();
        String nationality = GUIComponents.getCbxNationality()
                                .getSelectedItem().toString();
        String profession = GUIComponents.getCbxProfession()
                                .getSelectedItem().toString();

        if (firstName.isEmpty() || familyName.isEmpty()) {
            JOptionPane.showMessageDialog(null, "The first name and  family must be filled");
            return;
        }

        person = new Person(firstName, familyName, nationality, profession);

        // Write object to file
        try {
            objectOut.writeObject(person);
            objectOut.flush();
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        JOptionPane.showMessageDialog(null, "Person " + " was addded" );
        resetInputFields();
    }

    @Override
    public void openToRead() {
        String fileName = JOptionPane.showInputDialog("Enter the file name: ");

        if (fileName == null) {
            JOptionPane.showMessageDialog(null, "You do not open any file to read!");
            return;
        };

        if (!isFileExist(fileName)) {
            JOptionPane.showMessageDialog(null, "The file " + fileName +
                FILE_EXTENSION + " does not exist!");
            return;
        }

        // Setbuttons "Open for Writing" enabled and "Write this" disabled
        GUIComponents.getBtn(0).setEnabled(true);
        GUIComponents.getBtn(1).setEnabled(false);

        // Reset input fields, disable input fields
        resetInputFields();
        setInputFieldsEnabled(false);

        file = getFile(fileName);

        //Open new input stream
        try {
            objectIn = new ObjectInputStream(new FileInputStream(file));
            if (objectOut != null) {
                objectOut.close();
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }

        JOptionPane.showMessageDialog(null, "You are reading from"
                + " existing file at " + file.getAbsolutePath());

        // Enable button "Read next"
         GUIComponents.getBtn(3).setEnabled(true);
    }

    @Override
    public void readNext() {
        try{
            person = (Person)objectIn.readObject();
            String display = person.toString();
            GUIComponents.getTxtArea().setText(display);
        }
        catch (EOFException e) {
            GUIComponents.getTxtArea().setText("File End.\nOpen for reading or writing");

            // Close input stream
            try {
                objectIn.close();
            } catch (Exception e2) {
                e2.printStackTrace();
            }

            // Disable button "Read next"
            GUIComponents.getBtn(3).setEnabled(false);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Checks if the file name is valid.
     *
     * @param fileName the file name to check
     * @return true if the file name is valid, otherwise false
     */
    public boolean isFileNameValid(String fileName){
        boolean isFileNameValid = true;

        if (fileName == null || fileName.isEmpty()) {
            isFileNameValid = false;
        }

        return isFileNameValid;
    }

    /**
     * Returns the file with certain file name.
     *
     * @param fileName the file name to check
     * @return true if the file exists, false otherwise
     */
    public boolean isFileExist(String fileName){
        // Adding extension to a file name
       fileName += FILE_EXTENSION;
       // Get the path of the current directory
        String path = System.getProperty("user.dir");

        File file = new File(path + File.separator + fileName);

        return file.exists() && !file.isDirectory();
    }

    /**
     * Returns the file with certain file name.
     *
     * @param fileName the file name to check
     * @
     */
    public File getFile(String fileName){
        // Adding extension to a file name
	   fileName += FILE_EXTENSION;
       // Get the path of the current directory
        String path = System.getProperty("user.dir");

        File getFile = new File(path + File.separator + fileName);

        return getFile;
    }

    /**
     * Sets whether or not input fields are enabled.
     *
     * @param enabled - true if input fields should be enabled, false otherwise
     */
    public void setInputFieldsEnabled(boolean enabled) {
      GUIComponents.getTextfieldFirstName().setEnabled(true);
      GUIComponents.getTextfieldFamilyName().setEnabled(true);
      GUIComponents.getCbxNationality().setEnabled(true);
      GUIComponents.getCbxProfession().setEnabled(true);
    }

    /**
     * Resets input fields.
     */
    public void resetInputFields() {
      GUIComponents.getTextfieldFirstName().setText("");
      GUIComponents.getTextfieldFamilyName().setText("");
      GUIComponents.getCbxNationality().setSelectedIndex(0);
      GUIComponents.getCbxProfession().setSelectedIndex(0);
    }

}
