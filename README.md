# Overview

The simple java desktop application to write object to file and read object from file.

## How to run a program

Compile the program to create `JavaDesktopApp` directory

```sh
cd simple-java-desktop-app
javac . src/*.java
```

Run a program as follow:

```sh
# Method 1
java JavaDesktopApp.MainProgram

# Method 2
echo Main-Class: JavaDesktopApp.MainProgram > manifest.txt
jar cvfm JavaDesktopApp.jar manifest.txt JavaDesktopApp/*.class
java -jar JavaDesktopApp.jar
```



